/*
	Lesson Proper

	What is JavaScript?

		Javascript is a scripting programming language that enables us to make interactive web pages.

	Comments

		in JS there are two ways to add comments:
		// or ctrl + / - creates a single line comment.
*/
/*
	ctrl + shift + / - creates a multi line comment.
	Multi Line comments allows us to add multiple lines in a single comment.

*/
// single line comment - allows only for a single line of comment

console.log("Hello, World!");

/*
	console.log() allows us to show/display data in our console. The console is a part of our browser with which we can use to display data.
*/

/*
	Mini-Activity:

	Create 2 new console.log() and display the following in your browser console.

	Display your name. (first name or nickname)
	Display the name of your favorite food.

*/

console.log("Tee Jae");
console.log("Adobo");

/*
	Statement and Syntax

	Statements are instructions/expressions we add to our script/program which will then be communicated to our computers. Our computers will then be able to interpret these instructions and perform the task accordingly.
	
	Example:
	console.log("Tee Jae");
		We are actually the computer using JS to:
			"In the console, log this data."

	Most programming languages end their statements in semicolon(;). However, JS does not require a semi-colon.
	
	Syntax, in programming, is a set of rules that describes how statements or instructions are properly made/constructed.

	For your programs/script to work, we should be able to follow a certain set of rules.

	correct:
	console.log(<data>)
	
	wrong:
	(data)console.log
		In English:
		"This data, in the console, display"
*/
/*
	Variables

	In HTML, elements are containers for text and other elements.

	In JavaScript, variables are containers of data.

	This will then allow us to save data within our script or program.

	To create a variable, we use the let keyword and the assignment operator (=).

	Syntax:

	let variableName = "data";

*/

let name = "Jayson Gepayo";

//log the value of the variable in the console:
console.log(name);

//save numbers in variables:
let num = 5;
let num2 = 10;

//log the values of the variables in the console:
console.log(num);
console.log(num2);

//When console.logging variables, the name of the variables will not be shown, what will be shown/displayed are the contained within the variables.

//You could also check/display the values of mutiple variables:
//console.log(variable1,variable2);

console.log(name,num,num2);

//We cannot display the value of a variable that is not yet created/declared.
//In fact, that will result in an error. (not defined)
//console.log(name2);

let myVariable;
/*
	You can actually create variables without providing an initial value. However, that variable will be assigned as "undefined". Because we don't know the value yet, because we haven't provided a value yet. You can actually add the value later.

*/
console.log(myVariable);//result: undefined.
/*
	Creating variables is actually 2 steps:

	1. Declaration - It is the declaraction/creation of the variable with either the let or const keyword.
	
	2. Initialization - is when we provide an initial value to our variable.
	
	Declaration   Initialization
	let myVar  =  "initial value"

*/
//You can update/assign a value to a variable after it has been declared:
myVariable = "new value";
console.log(myVariable);

let bestFinalFantasy = "Final Fantasy X";
console.log(bestFinalFantasy);

//You can update the value of a variable declared using the let keyword
bestFinalFantasy = "Final Fantasy 7";
console.log(bestFinalFantasy);

//We cannot update with the let keyword.
//We cannot create another variable with the same name.
//let bestFinalFantasy = "Final Fantasy 6";
//console.log(bestFinalFantasy);

/*
	Const

	const keyword allows us to create a variable like let, however, the const variable cannot be updated. Values in a const variable cannot be changed. You also cannot create a const variable without initialization.

*/

const pi = 3.1416;
console.log(pi);

//variables declared with const cannot be updated/re-assigned.
//pi = 3.15;
//console.log(pi);

//Declare const variable without initialization results to an error.
//const plateNum;
//console.log(plateNum);

let name2 = "Edward Cullen";
let role = "Supervisor";

/*
	Mini-Activity:

	1. Update the value of the role variable to "Director".
	2. Create a new const variable called tin with an initial value of "12333-1234".
	3. Display name2,role and tin variables in a single console.log()

*/

role = "Director";
const tin = "12333-1234";
console.log(name2,role,tin);
/*
	Conventions in creating variable/constant names:

	To create a let variable, we use the let keyword, to create a const variable, we use the const keyword.

	variables declared with let, its value can be updated. variables declared with const, we cannot change the value.

	let variables and const variables are usually named in small caps. Because there are other JS keywords that start in capital letters.

	If you want to name your variables with multiple words, we can use camelCase. camelCase is a convention in writing variables by adding the first word in small caps and the following words starting with capital letters.

	variable names define the values they contain. Name your variables appropriately.

*/

let user = "09266772400";
let carBrand = "Despacito";

//Data Types

/*
	In most programming languages, data is differentiated into different types and we can do different things about this data. For most programming languages, we have to declare the data type of our data before we are able to create the variable and store it.
	
	However, JS is not strict when it comes to data types.

	There are data types wherein we need to use literals to create them:
	To create strings, we use string literals like '' or ""
	To create objects, we can use object literals like {}
	To create arrays, we can use array literals like []

*/

/*

	Strings

	Strings are a series of alphanumeric characters that create a word, a phrase, a name or anything that is related to creating a text.

	Strings are NOT and should not be used for mathematical operations.

	Strings are created with string literals like single quotes('') or double quotes("")

*/

let country = "Philippines";
let province = "Metro Manila";

console.log(province, country);

/*
	You can actually combine strings into a single string with the use of the plus sign or addition operator (+).

	This process is called concatenation.

*/

/*In strings, spaces and commas count as characters*/
let address = province + ", " + country;
console.log(address);

let city1 = "Manila";
let city2 = "Copenhagen";
let city3 = "Washington D.C.";
let city4 = "Tokyo";
let city5 = "New York";
let country1 = "Philippines";
let country2 = "U.S.A.";
let country3 = "South Korea";
let country4 = "Japan";

/*
	Mini-Activity

	Combine the strings to match the capital cities to their respective countries. 

	Create the following variables and display its value in the console:

	capital1 = show the capital of the Philippines. 
	capital2 = show the capital of the USA.
	capital3 = show the capital of Japan.

	As shown in the console:
	"city, country"
	
	Log each variable separately.

*/

let capital1 = city1 + ", " + country1;
let capital2 = city3 + ", " + country2;
let capital3 = city4 + ", " + country4;

console.log(capital1);
console.log(capital2);
console.log(capital3);

/*Numbers/Integers*/
//Integers are number data which can be used for mathematical operations.
//To create a number data, add a numeric character but with no "" or ''

let number1 = 10;
let number2 = 6;

console.log(number1);
console.log(number2);

/*
	Addition Operator

	(+) when used between 2 number types, it will add both numbers. The result of the addition can also be saved in a variable.

*/

let sum1 = number1 + number2;
console.log(sum1);//16

let sum2 = 16 + 4;
console.log(sum2);//20

let numString1 = "30";
let numString2 = "50";
/*These are numeric strings, they are composed numeric characters but are considered a string because it is surrounded/created with double quotes("")*/

let sumString1 = numString1 + numString2;
console.log(sumString1);
//When numeric strings are used with addition operator(+), it will not add but only combine the strings, it will concatenate

//Note: When a number/integer is added with a numeric string, it results to concatenation. 
let sum3 = number1 + numString2;
console.log(sum3);//1050

/*Boolean (true or false)*/
	/*
		Boolean is usually used for logical operations and if-else conditions.

		When creating a variable that holds a boolean, the variable name is usually a yes or no question.

	*/

	let isAdmin = true;
	let isMarried = false;
	let isMVP = true;

	console.log("Is she married? " + isMarried);
	console.log("Is Curry an MVP? " + isMVP);
	console.log("Is he the current admin? " + isAdmin);

/*Arrays*/

	//Arrays are a special kind of data type. It is used to store multiple values.
	
	//Arrays can actually store values of different data type, however, this is not a good practice, because there are array methods or ways to manipulate array which might be affect or cause conflict.

	//Arrays are usually used to store multiple values of the same data type.
	//There are only few use cases wherein arrays are used with multiple values of different types.

	//Arrays are created using an array literal or square brackets []
	//Values in an array are separated by a comma.

	//This is a good array which:
		//The values have the same data type.
		//The values share the same theme.

	let array1 = ["Goku","Gohan","Goten","Vegeta","Trunks","Broly"];
	let array2 = ["One Punch Man","Saitama",true,5000];

	console.log(array1);
	console.log(array2);

	/*In the next sessions, we will learn about methods which will allow us to manipulate an array. Having an array with different data types may obstruct to these methods.*/

	//Arrays are best thought of as a group of data.

/*Objects*/

	/*
		Objects are another special kind of data type used to mimic real world items/objects.

		It is used to create complex data structure that contain pieces of information that relate to each other.

		Each field in an object is called a property. Each properties are separated by comma.

		Each property is a pair of key: value

		Objects can actually group different data types.

		Each data is given more significance because each data/value has key which defines/describes the data.
	*/

	let hero1 = {

		heroName: "One Punch Man",
		realName: "Saitama",
		income: 5000,
		isActive: true

	}

	/*
		Mini-Activity:

		Create a variable with a group of data with the same data type.
			-The group of data should contain the names of the members of your favorite band.
			-Name the variable as the name of your favorite band.

		Create a variable which can contain multiple values with different data types and describes a single object/person:

			It should have the following pairs:

			firstName: <value>,
			lastName: <value>,
			isDeveloper: <value>,
			age: <value>

		Log both variables in the console.

	*/

	let beatles = ["Ringo","Paul","John","George"];
	let person1 = {
		firstName: "Paul",
		lastName: "Martin",
		isDeveloper: true,
		age: 25
	};
	console.log(beatles);
	console.log(person1);


/*Null and Undefined*/

//Null is the explicit absence of data/value. This is done to show that a variable actually contains nothing as opposed to undefined which means that the variable has been declared or create but there was no initial value.
	//Null explicit absence value.
	//Undefined there is no value YET.

	//Use Cases of Null

	//When doing a query or search, there, of course might a 0 result.
		let foundResult = null;

//Undefined is a representation that a variable has been create or declared but there is no initial value, so, we can't quite say what the value is, thus it is undefined. 
	
		let sampleVariable;//declaration with no initial value results to undefined

	//For undefined, this is normally caused by developers when creating variables that have no data or value associated or initialized with them.

		let person2 = {
			name: "Peter",
			age:42
		}

		//Access or display the value of an object's property we use dot notation:
		//objectName.propertyName
		
		console.log(person2.name);
		console.log(person2.age);

		//Undefined, because the the person2 variable does exist, however, there is no property in the object called isAdmin.
		console.log(person2.isAdmin);

/*Functions*/

/*
	Functions in JS, are lines/blocks of code that tell our device/application to perform a certain task when called or invoked.

	Functions are reusable pieces of code with instructions/statements which can be used over and over again just so long as we call or use it.

	You can think of functions as programs within a program.

*/

	console.log("Good Afternoon, Everyone! Welcome to my application!");

	//functions are created by declaring the function using the function keyword.
	function greet(){

		console.log("Good Afternoon, Everyone! Welcome to my application!");

	}

	//function invocation - function invocation is when we call or use our functions.
	greet();
	greet();
	greet(); 


let favFood = "Adobo";
let sum = 150 + 9;
let product = 100 * 90;
let isActive = true;


let favRestaurant = ["Casa Velin", "Alavar", "Palmeras","Tito Jiggs","Hanazono"];


let person = {
    firstName: "Daniel Grayling",
    lastName: " fogelberg",
    stageName: "Dan Fogelberg",
    birthDay: "August 13, 1951",
    age: 56,
    bestAlbum: "The Innocent Age",
    bestTvShow: null,
    bestMovie: null,
    isActive: false
}

console.log(favFood);
console.log(sum);
console.log(product);
console.log(isActive);
console.log(favRestaurant);
console.log(person);


//Parameters and Arguments
// "name is called a parameter"
//Parameter acts as a named variable/container that exists ONLY in the function. this is used to store information to act as a stand-in or the container of the value passed ionto the function as an argument.



function printName (name) {
    console.log(`My Name is ${name}`);
}
// console.log(name)

// data passed in the function: argument
//Representation of the argument within the function: parameter
printName("Jake");

function displayNum (number) {
    console.log(number);
}

displayNum(3001);
displayNum(3001);

function displayMsg(msg){
    console.log(msg);
}

displayMsg("JavaScript is Fun");

//Multiple Parameters and Arguments

function displayFullName (firstName, lastName, age)
{
    console.log(`${firstName} ${lastName} is ${age}`)

    // console.log(firstName, lastName, "is", age);
}

displayFullName("Jayson", "Gepayo", 28)

// return keyword - return keyword is used so that a function may return a value
//it also stops the process of the function after the return keyword

function createFullName(firstName, middleName, lastName){
   return `${firstName}, ${middleName}, ${lastName}`
   console.log("I will no Longer run because the function's value / result has been returned")
}
let fullName1 = createFullName("Tom","Cruise", "Mapother");

console.log(fullName1);

